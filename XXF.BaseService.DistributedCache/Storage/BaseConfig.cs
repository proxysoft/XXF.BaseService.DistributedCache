﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.DistributedCache.Storage
{
    public class BaseConfig
    {
        public int MaxPoolSize = 100;

        public static bool CompressesFlag = true;
        public virtual void Parse(string config)
        {

        }
    }
}
